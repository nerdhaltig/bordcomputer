#!/bin/bash
cd /build

# register qemu-static-binarys to support cross-build
/register --reset -p yes

# build the sd-image.nix
nix-build '<nixpkgs/nixos>' -A config.system.build.sdImage --argstr system aarch64-linux -I nixos-config=./sd-image.nix

# get the path where the new sd-image is found
result=$(nix-build '<nixpkgs/nixos>' -A config.system.build.sdImage --argstr system aarch64-linux -I nixos-config=./sd-image.nix)

# copy it to your project path
cp -r $result /build/$(basename ${result})
