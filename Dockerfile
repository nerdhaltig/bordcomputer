FROM multiarch/qemu-user-static as qemu

FROM nixos/nix

# This will cache some nix-derivations to use it later
RUN echo "extra-platforms = aarch64-linux" >> /etc/nix/nix.conf

# copy qemu stuff
COPY --from=qemu /register /register
COPY --from=qemu /qemu-binfmt-conf.sh /qemu-binfmt-conf.sh
COPY --from=qemu /usr/bin/qemu-*-static /usr/bin/

RUN nix-channel --add https://nixos.org/channels/nixos-21.11 nixpkgs && \
    nix-channel --update

RUN nix-env -iA nixpkgs.bash

COPY build.bash /usr/bin/build.bash

CMD [ "bash", "/usr/bin/build.bash" ]