# The bordcomputer on my sailing-vessle

This is the sd-card-image-builder for the bordcomputer on my sailing-vessle.
Basically this project support build on aarch64 and cross-build on x86_64 based hosts.

That works:

- ✅ based on NixOS 21.11
- 🏗 build on aarch64
- 🏗 build on x86_64
- 🧺 caching the nix-store on docker-volume for rebuilds

ToDo:

- ✖ Test if the image works on RPi4

## build

```
docker-compose up --build sd-image
```

or if you are on nixos:

```
nix-shell -p docker-compose --command "docker-compose up --build sd-image"
```

## customize

You can customize you nix-based sd-image, just change the `sd-image.nix`

## How this works

Basically, we copy the static-compiled qemu-binarys from the qemu-docker-image to an nixos/nix-docker-image.

With this new created docker-image, we start to build our sd-card-image ( `build.bash` ) with nix and use the `sd-image.nix`. Thats all, easy-peasy.

